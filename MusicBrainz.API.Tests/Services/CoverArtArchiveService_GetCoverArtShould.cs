using Moq;
using Moq.Protected;
using MusicBrainz.API.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MusicBrainz.API.Tests.Services
{
    public class CoverArtArchive_GetCoverArtShould
    {
        [Fact]
        public async Task ReturnNullWhenStatusCodeIsNotOk()
        {
            // Assign
            var message = new HttpResponseMessage(HttpStatusCode.NotFound);
            var coverArtService = CreateService(message);

            // Act
            var result = await coverArtService.GetCoverArt("a");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task ReturnsCoverArtWhenStatusIsOk()
        {
            // Assign
            var data = @"
            {
                ""images"": [
                    {
                        ""types"": [
                            ""Front""
                        ],
                        ""front"": true,
                        ""back"": false,
                        ""edit"": 1234,
                        ""image"": ""http://localhost/image.jpg"",
                        ""comment"": ""a"",
                        ""approved"": true,
                        ""thumbnails"": {
                            ""large"": ""http://localhost/image-large.jpg"",
                            ""small"": ""http://localhost/image-small.jpg""
                        },
                        ""id"": ""5678""
                    }
                ],
                ""release"": ""http://localhost/release""
            }";
            var message = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data, Encoding.UTF8, "application/json"),
            };
            var coverArtService = CreateService(message);

            // Act
            var result = await coverArtService.GetCoverArt("a");

            // Assert
            Assert.NotNull(result);
            Assert.Collection(result.Images,
                item =>
                {
                    Assert.Collection(item.Types, type => Assert.Equal("Front", type));
                    Assert.True(item.Front);
                    Assert.False(item.Back);
                    Assert.Equal(1234, item.Edit);
                    Assert.Equal("http://localhost/image.jpg", item.ImageUrl.ToString());
                    Assert.Equal("a", item.Comment);
                    Assert.True(item.Approved);
                    Assert.Equal("http://localhost/image-large.jpg", item.Thumbnails["large"].ToString());
                    Assert.Equal("http://localhost/image-small.jpg", item.Thumbnails["small"].ToString());
                    Assert.Equal("5678", item.Id);
                }
            );
            Assert.Equal("http://localhost/release", result.Release.ToString());
        }

        private ICoverArtArchiveService CreateService(HttpResponseMessage message)
        {
            var handler = new Mock<HttpMessageHandler>();
            handler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>()).Returns(Task.FromResult(message));
            var client = new HttpClient(handler.Object);
            client.BaseAddress = new Uri("http://localhost/");

            return new CoverArtArchiveService(client);
        }
    }
}
