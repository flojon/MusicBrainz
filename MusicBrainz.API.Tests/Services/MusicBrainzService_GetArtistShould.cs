using Xunit;
using MusicBrainz.API.Services;
using Moq;
using Moq.Protected;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System;
using System.Threading;
using System.Text;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Tests.Services
{
    public class MusicBrainzService_GetArtistShould
    {
        [Fact]
        public async Task ReturnNullWhenStatusCodeIsNotOk()
        {
            // Assign
            var message = new HttpResponseMessage(HttpStatusCode.NotFound);
            var mbService = CreateService(message);

            // Act
            var result = await mbService.GetArtist("a");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task ReturnsArtistWhenStatusIsOk()
        {
            // Assign
            var data = @"
            {
                ""id"": ""a"",
                ""name"": ""b"",
                ""country"": ""c"",
                ""sort-name"": ""d"",
                ""type"": ""e"",
                ""gender"": ""f"",
                ""life-span"": {
                    ""ended"": true,
                    ""end"": ""g"",
                    ""begin"": ""h"",
                },
                ""relations"": [
                    {
                        ""type-id"": ""i"",
                        ""target-type"": ""j"",
                        ""url"": {
                            ""resource"": ""k"",
                            ""id"": ""o""
                        },
                        ""direction"": ""l"",
                        ""ended"": false,
                        ""begin"": ""m"",
                        ""end"": ""n""
                    }
                ],
                ""release-groups"": [
                    {
                        ""title"": ""p"",
                        ""primary-type"": ""q"",
                        ""secondary-type-ids"": [""v""],
                        ""secondary-types"": [""w""],
                        ""disambiguation"": ""r"",
                        ""id"": ""s"",
                        ""first-release-date"": ""t"",
                        ""primary-type-id"": ""u"",
                    }
                ]
            }";
            var message = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data, Encoding.UTF8, "application/json"),
            };
            var mbService = CreateService(message);

            // Act
            var result = await mbService.GetArtist("a");

            // Assert
            Assert.NotNull(result);
            Assert.Equal("a", result.Id);
            Assert.Equal("b", result.Name);
            Assert.Equal("c", result.Country);
            Assert.Equal("d", result.SortName);
            Assert.Equal("e", result.Type);
            Assert.Equal("f", result.Gender);
            Assert.True(result.LifeSpan.Ended);
            Assert.Equal("g", result.LifeSpan.End);
            Assert.Equal("h", result.LifeSpan.Begin);
            Assert.Collection<Relation>(result.Relations,
                item =>
                {
                    Assert.Equal("i", item.TypeId);
                    Assert.Equal("j", item.TargetType);
                    Assert.Equal("k", item.Url.Resource);
                    Assert.Equal("o", item.Url.Id);
                    Assert.Equal("l", item.Direction);
                    Assert.False(item.Ended);
                    Assert.Equal("m", item.Begin);
                    Assert.Equal("n", item.End);
                }
            );
            Assert.Collection<ReleaseGroup>(result.ReleaseGroups,
                item =>
                {
                    Assert.Equal("p", item.Title);
                    Assert.Equal("q", item.PrimaryType);
                    Assert.Collection<string>(item.SecondaryTypeIds, subItem => Assert.Equal("v", subItem));
                    Assert.Collection<string>(item.SecondaryTypes, subItem => Assert.Equal("w", subItem));
                    Assert.Equal("r", item.Disambiguation);
                    Assert.Equal("s", item.Id);
                    Assert.Equal("t", item.FirstReleaseDate);
                    Assert.Equal("u", item.PrimaryTypeId);
                }
            );
        }

        private MusicBrainzService CreateService(HttpResponseMessage message)
        {
            var handler = new Mock<HttpMessageHandler>();
            handler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>()).Returns(Task.FromResult(message));
            var client = new HttpClient(handler.Object);
            client.BaseAddress = new Uri("http://localhost/");

            return  new MusicBrainzService(client);
        }
    }
}
