using Xunit;
using MusicBrainz.API.Services;
using Moq;
using Moq.Protected;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System;
using System.Threading;
using System.Text;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Tests.Services
{
    public class WikipediaService_GetExtractShould
    {
        [Fact]
        public async Task ReturnNullWhenStatusCodeIsNotOk()
        {
            // Assign
            var message = new HttpResponseMessage(HttpStatusCode.NotFound);
            var wikiService = CreateService(message);

            // Act
            var result = await wikiService.GetExtract("a");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task ReturnsArtistExtractWhenStatusIsOk()
        {
            // Assign
            var data = @"
            {
                ""batchcomplete"": ""a"",
                ""warnings"": {
                    ""extracts"": {
                        ""*"": ""b""
                    }
                },
                ""query"": {
                    ""normalized"": [
                        {
                            ""from"": ""c"",
                            ""to"": ""d""
                        }
                    ],
                    ""pages"": {
                        ""e"": {
                            ""pageid"": ""e"",
                            ""ns"": 0,
                            ""title"": ""f"",
                            ""extract"": ""g""
                        }
                    }
                }
            }";
            var message = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data, Encoding.UTF8, "application/json"),
            };
            var wikiService = CreateService(message);

            // Act
            var result = await wikiService.GetExtract("a");

            // Assert
            Assert.NotNull(result);
            Assert.Equal("a", result.Batchcomplete);
            Assert.NotNull(result.Warnings);
            Assert.NotNull(result.Warnings["extracts"]);
            Assert.NotNull(result.Warnings["extracts"]["*"]);
            Assert.Equal("b", result.Warnings["extracts"]["*"]);
            Assert.NotNull(result.Query);
            Assert.Collection(result.Query.Normalized,
                item =>
                {
                    Assert.Equal("c", item.From);
                    Assert.Equal("d", item.To);
                }
            );
            Assert.NotNull(result.Query.Pages);
            Assert.NotNull(result.Query.Pages["e"]);
            Assert.Equal("e", result.Query.Pages["e"].PageId);
            Assert.Equal(0, result.Query.Pages["e"].Ns);
            Assert.Equal("f", result.Query.Pages["e"].Title);
            Assert.Equal("g", result.Query.Pages["e"].Extract);
        }

        private IWikipediaService CreateService(HttpResponseMessage message)
        {
            var handler = new Mock<HttpMessageHandler>();
            handler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>()).Returns(Task.FromResult(message));
            var client = new HttpClient(handler.Object);
            client.BaseAddress = new Uri("http://localhost/");

            return new WikipediaService(client);
        }
    }
}
