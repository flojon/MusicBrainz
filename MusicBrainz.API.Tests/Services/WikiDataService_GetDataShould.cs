using Xunit;
using MusicBrainz.API.Services;
using Moq;
using Moq.Protected;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System;
using System.Threading;
using System.Text;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Tests.Services
{
    public class WikiDataService_GetDataShould
    {
        [Fact]
        public async Task ReturnOkWhenStatusIsOk()
        {
            // Assign
            var data = @"
                {
                    ""entities"": {
                        ""Q11649"": {
                            ""pageid"": 13180,
                            ""ns"": 0,
                            ""title"": ""Q11649"",
                            ""lastrevid"": 940218571,
                            ""modified"": ""2019-05-12T12:45:36Z"",
                            ""type"": ""item"",
                            ""id"": ""Q11649"",
                            ""labels"": {
                                ""en"": {
                                    ""language"": ""en"",
                                    ""value"": ""Nirvana""
                                },
                                ""fr"": {
                                    ""language"": ""fr"",
                                    ""value"": ""Nirvana""
                                },
                                ""ru"": {
                                    ""language"": ""ru"",
                                    ""value"": ""Nirvana""
                                },
                                ""ca"": {
                                    ""language"": ""ca"",
                                    ""value"": ""Nirvana""
                                },
                                ""de"": {
                                    ""language"": ""de"",
                                    ""value"": ""Nirvana""
                                },
                                ""ang"": {
                                    ""language"": ""ang"",
                                    ""value"": ""Nirvana""
                                },
                                ""an"": {
                                    ""language"": ""an"",
                                    ""value"": ""Nirvana""
                                },
                                ""ar"": {
                                    ""language"": ""ar"",
                                    ""value"": ""نيرفانا""
                                },
                                ""ast"": {
                                    ""language"": ""ast"",
                                    ""value"": ""Nirvana""
                                },
                                ""az"": {
                                    ""language"": ""az"",
                                    ""value"": ""Nirvana""
                                },
                                ""bar"": {
                                    ""language"": ""bar"",
                                    ""value"": ""Nirvana""
                                },
                                ""bg"": {
                                    ""language"": ""bg"",
                                    ""value"": ""Нирвана""
                                },
                                ""br"": {
                                    ""language"": ""br"",
                                    ""value"": ""Nirvana""
                                },
                                ""bs"": {
                                    ""language"": ""bs"",
                                    ""value"": ""Nirvana""
                                },
                                ""co"": {
                                    ""language"": ""co"",
                                    ""value"": ""Nirvana""
                                },
                                ""csb"": {
                                    ""language"": ""csb"",
                                    ""value"": ""Nirvana""
                                },
                                ""cs"": {
                                    ""language"": ""cs"",
                                    ""value"": ""Nirvana""
                                },
                                ""cy"": {
                                    ""language"": ""cy"",
                                    ""value"": ""Nirvana""
                                },
                                ""da"": {
                                    ""language"": ""da"",
                                    ""value"": ""Nirvana""
                                },
                                ""el"": {
                                    ""language"": ""el"",
                                    ""value"": ""Nirvana""
                                },
                                ""eml"": {
                                    ""language"": ""eml"",
                                    ""value"": ""Nirvana""
                                },
                                ""en-ca"": {
                                    ""language"": ""en-ca"",
                                    ""value"": ""Nirvana""
                                },
                                ""en-gb"": {
                                    ""language"": ""en-gb"",
                                    ""value"": ""Nirvana""
                                },
                                ""eo"": {
                                    ""language"": ""eo"",
                                    ""value"": ""Nirvana""
                                },
                                ""es"": {
                                    ""language"": ""es"",
                                    ""value"": ""Nirvana""
                                },
                                ""et"": {
                                    ""language"": ""et"",
                                    ""value"": ""Nirvana""
                                },
                                ""ext"": {
                                    ""language"": ""ext"",
                                    ""value"": ""Nirvana""
                                },
                                ""fa"": {
                                    ""language"": ""fa"",
                                    ""value"": ""نیروانا""
                                },
                                ""fi"": {
                                    ""language"": ""fi"",
                                    ""value"": ""Nirvana""
                                },
                                ""fur"": {
                                    ""language"": ""fur"",
                                    ""value"": ""Nirvana""
                                },
                                ""ga"": {
                                    ""language"": ""ga"",
                                    ""value"": ""Nirvana""
                                },
                                ""gl"": {
                                    ""language"": ""gl"",
                                    ""value"": ""Nirvana""
                                },
                                ""he"": {
                                    ""language"": ""he"",
                                    ""value"": ""נירוונה""
                                },
                                ""hr"": {
                                    ""language"": ""hr"",
                                    ""value"": ""Nirvana""
                                },
                                ""hu"": {
                                    ""language"": ""hu"",
                                    ""value"": ""Nirvana""
                                },
                                ""hy"": {
                                    ""language"": ""hy"",
                                    ""value"": ""Նիրվանա""
                                },
                                ""id"": {
                                    ""language"": ""id"",
                                    ""value"": ""Nirvana""
                                },
                                ""is"": {
                                    ""language"": ""is"",
                                    ""value"": ""Nirvana""
                                },
                                ""it"": {
                                    ""language"": ""it"",
                                    ""value"": ""Nirvana""
                                },
                                ""ja"": {
                                    ""language"": ""ja"",
                                    ""value"": ""ニルヴァーナ""
                                },
                                ""ka"": {
                                    ""language"": ""ka"",
                                    ""value"": ""ნირვანა""
                                },
                                ""ko"": {
                                    ""language"": ""ko"",
                                    ""value"": ""너바나""
                                },
                                ""la"": {
                                    ""language"": ""la"",
                                    ""value"": ""Nirvana""
                                },
                                ""lt"": {
                                    ""language"": ""lt"",
                                    ""value"": ""Nirvana""
                                },
                                ""lv"": {
                                    ""language"": ""lv"",
                                    ""value"": ""Nirvana""
                                },
                                ""mk"": {
                                    ""language"": ""mk"",
                                    ""value"": ""Nirvana""
                                },
                                ""mn"": {
                                    ""language"": ""mn"",
                                    ""value"": ""Нирвана""
                                },
                                ""ms"": {
                                    ""language"": ""ms"",
                                    ""value"": ""Nirvana""
                                },
                                ""nah"": {
                                    ""language"": ""nah"",
                                    ""value"": ""Nirvana""
                                },
                                ""nl"": {
                                    ""language"": ""nl"",
                                    ""value"": ""Nirvana""
                                },
                                ""nn"": {
                                    ""language"": ""nn"",
                                    ""value"": ""Rockegruppa Nirvana""
                                },
                                ""oc"": {
                                    ""language"": ""oc"",
                                    ""value"": ""Nirvana""
                                },
                                ""pdc"": {
                                    ""language"": ""pdc"",
                                    ""value"": ""Nirvana""
                                },
                                ""pl"": {
                                    ""language"": ""pl"",
                                    ""value"": ""Nirvana""
                                },
                                ""pms"": {
                                    ""language"": ""pms"",
                                    ""value"": ""Nirvana""
                                },
                                ""pt"": {
                                    ""language"": ""pt"",
                                    ""value"": ""Nirvana""
                                },
                                ""pt-br"": {
                                    ""language"": ""pt-br"",
                                    ""value"": ""Nirvana""
                                },
                                ""ro"": {
                                    ""language"": ""ro"",
                                    ""value"": ""Nirvana""
                                },
                                ""scn"": {
                                    ""language"": ""scn"",
                                    ""value"": ""Nirvana""
                                },
                                ""sh"": {
                                    ""language"": ""sh"",
                                    ""value"": ""Nirvana""
                                },
                                ""sk"": {
                                    ""language"": ""sk"",
                                    ""value"": ""Nirvana""
                                },
                                ""sl"": {
                                    ""language"": ""sl"",
                                    ""value"": ""Nirvana""
                                },
                                ""sq"": {
                                    ""language"": ""sq"",
                                    ""value"": ""Nirvana""
                                },
                                ""sr"": {
                                    ""language"": ""sr"",
                                    ""value"": ""Нирвана""
                                },
                                ""sv"": {
                                    ""language"": ""sv"",
                                    ""value"": ""Nirvana""
                                },
                                ""szl"": {
                                    ""language"": ""szl"",
                                    ""value"": ""Nirvana""
                                },
                                ""th"": {
                                    ""language"": ""th"",
                                    ""value"": ""เนอร์วานา""
                                },
                                ""tr"": {
                                    ""language"": ""tr"",
                                    ""value"": ""Nirvana""
                                },
                                ""uk"": {
                                    ""language"": ""uk"",
                                    ""value"": ""Nirvana""
                                },
                                ""uz"": {
                                    ""language"": ""uz"",
                                    ""value"": ""Nirvana""
                                },
                                ""vi"": {
                                    ""language"": ""vi"",
                                    ""value"": ""Nirvana""
                                },
                                ""zh"": {
                                    ""language"": ""zh"",
                                    ""value"": ""涅槃乐队""
                                },
                                ""hsb"": {
                                    ""language"": ""hsb"",
                                    ""value"": ""Nirvana""
                                },
                                ""nb"": {
                                    ""language"": ""nb"",
                                    ""value"": ""Nirvana""
                                },
                                ""nds"": {
                                    ""language"": ""nds"",
                                    ""value"": ""Nirvana""
                                },
                                ""be"": {
                                    ""language"": ""be"",
                                    ""value"": ""Nirvana""
                                },
                                ""sco"": {
                                    ""language"": ""sco"",
                                    ""value"": ""Nirvana""
                                },
                                ""mg"": {
                                    ""language"": ""mg"",
                                    ""value"": ""Nirvana""
                                },
                                ""diq"": {
                                    ""language"": ""diq"",
                                    ""value"": ""Nirvana""
                                },
                                ""kk"": {
                                    ""language"": ""kk"",
                                    ""value"": ""Nirvana""
                                },
                                ""ne"": {
                                    ""language"": ""ne"",
                                    ""value"": ""निर्भाना""
                                },
                                ""be-tarask"": {
                                    ""language"": ""be-tarask"",
                                    ""value"": ""Nirvana""
                                },
                                ""bn"": {
                                    ""language"": ""bn"",
                                    ""value"": ""nirvana""
                                },
                                ""tl"": {
                                    ""language"": ""tl"",
                                    ""value"": ""Nirvana""
                                },
                                ""tt"": {
                                    ""language"": ""tt"",
                                    ""value"": ""Nirvana""
                                },
                                ""sc"": {
                                    ""language"": ""sc"",
                                    ""value"": ""Nirvana""
                                },
                                ""kw"": {
                                    ""language"": ""kw"",
                                    ""value"": ""Nirvana""
                                },
                                ""su"": {
                                    ""language"": ""su"",
                                    ""value"": ""Nirvana""
                                },
                                ""io"": {
                                    ""language"": ""io"",
                                    ""value"": ""Nirvana""
                                },
                                ""eu"": {
                                    ""language"": ""eu"",
                                    ""value"": ""Nirvana""
                                },
                                ""jv"": {
                                    ""language"": ""jv"",
                                    ""value"": ""Nirvana""
                                },
                                ""ml"": {
                                    ""language"": ""ml"",
                                    ""value"": ""നിർവാണ""
                                },
                                ""ckb"": {
                                    ""language"": ""ckb"",
                                    ""value"": ""نیرڤانا (گرووپ)""
                                },
                                ""azb"": {
                                    ""language"": ""azb"",
                                    ""value"": ""نیروانا""
                                },
                                ""xmf"": {
                                    ""language"": ""xmf"",
                                    ""value"": ""ნირვანა""
                                },
                                ""yue"": {
                                    ""language"": ""yue"",
                                    ""value"": ""涅槃樂隊""
                                },
                                ""zh-hant"": {
                                    ""language"": ""zh-hant"",
                                    ""value"": ""Nirvana；超脫樂團""
                                },
                                ""zh-cn"": {
                                    ""language"": ""zh-cn"",
                                    ""value"": ""Nirvana；超脱乐团""
                                },
                                ""got"": {
                                    ""language"": ""got"",
                                    ""value"": ""𐌽𐌹𐍂𐌱𐌰𐌽𐌰""
                                },
                                ""ky"": {
                                    ""language"": ""ky"",
                                    ""value"": ""Nirvana""
                                },
                                ""pcd"": {
                                    ""language"": ""pcd"",
                                    ""value"": ""Nirvana (grope)""
                                },
                                ""war"": {
                                    ""language"": ""war"",
                                    ""value"": ""Nirvana (banda)""
                                }
                            },
                            ""descriptions"": {
                                ""en"": {
                                    ""language"": ""en"",
                                    ""value"": ""American rock band""
                                },
                                ""it"": {
                                    ""language"": ""it"",
                                    ""value"": ""gruppo musicale statunitense""
                                },
                                ""es"": {
                                    ""language"": ""es"",
                                    ""value"": ""grupo de música grunge estadounidense""
                                },
                                ""nl"": {
                                    ""language"": ""nl"",
                                    ""value"": ""Amerikaanse band""
                                },
                                ""ja"": {
                                    ""language"": ""ja"",
                                    ""value"": ""アメリカ合衆国のロックバンド""
                                },
                                ""fr"": {
                                    ""language"": ""fr"",
                                    ""value"": ""groupe de rock américain""
                                },
                                ""de"": {
                                    ""language"": ""de"",
                                    ""value"": ""US-amerikanische Rockband""
                                },
                                ""sco"": {
                                    ""language"": ""sco"",
                                    ""value"": ""American rock baund""
                                },
                                ""fi"": {
                                    ""language"": ""fi"",
                                    ""value"": ""yhdysvaltalainen yhtye""
                                },
                                ""ko"": {
                                    ""language"": ""ko"",
                                    ""value"": ""미국의 록 밴드""
                                },
                                ""sv"": {
                                    ""language"": ""sv"",
                                    ""value"": ""amerikanskt rockband""
                                },
                                ""pt"": {
                                    ""language"": ""pt"",
                                    ""value"": ""Banda grunge de Seattle""
                                },
                                ""uk"": {
                                    ""language"": ""uk"",
                                    ""value"": ""американський рок-гурт""
                                },
                                ""fa"": {
                                    ""language"": ""fa"",
                                    ""value"": ""گروه راک آمریکایی""
                                },
                                ""sk"": {
                                    ""language"": ""sk"",
                                    ""value"": ""americká grunge rocková kapela""
                                },
                                ""he"": {
                                    ""language"": ""he"",
                                    ""value"": ""להקת גראנג' אמריקאית""
                                },
                                ""zh"": {
                                    ""language"": ""zh"",
                                    ""value"": ""Nirvana乐队的主唱兼吉他手""
                                },
                                ""vi"": {
                                    ""language"": ""vi"",
                                    ""value"": ""ban nhạc rock người Mỹ""
                                },
                                ""nb"": {
                                    ""language"": ""nb"",
                                    ""value"": ""amerikansk band""
                                }
                            },
                            ""aliases"": {
                                ""th"": [
                                    {
                                        ""language"": ""th"",
                                        ""value"": ""Nirvana""
                                    }
                                ],
                                ""ar"": [
                                    {
                                        ""language"": ""ar"",
                                        ""value"": ""نرفانا (فرقة)""
                                    },
                                    {
                                        ""language"": ""ar"",
                                        ""value"": ""فرقة نيرفانا""
                                    }
                                ]
                            },
                            ""claims"": {
                                ""P358"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P358"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 915525,
                                                    ""id"": ""Q915525""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$2ABFA53F-3310-4BF6-BCD9-CFC0776A1BDB"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""fa278ebfc458360e5aed63d5058cca83c46134f1"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 328,
                                                                    ""id"": ""Q328""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P373"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P373"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana (American band)"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""string""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$10EEF161-AA54-4801-8087-95E854FA417C"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""fa278ebfc458360e5aed63d5058cca83c46134f1"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 328,
                                                                    ""id"": ""Q328""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P244"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P244"",
                                            ""datavalue"": {
                                                ""value"": ""n92011111"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$64036935-BB4E-4940-907A-35E794C0BB38"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""fa278ebfc458360e5aed63d5058cca83c46134f1"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 328,
                                                                    ""id"": ""Q328""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P227"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P227"",
                                            ""datavalue"": {
                                                ""value"": ""10295339-9"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$E2E0757F-6EDD-4E0A-AEC2-5DE570AAB0D5"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""9a24f7c0208b05d6be97077d855671d1dfdbc0dd"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 48183,
                                                                    ""id"": ""Q48183""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P434"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P434"",
                                            ""datavalue"": {
                                                ""value"": ""5b11f4ce-a62d-471e-81fc-a69a8278c7da"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$0E3EA6B8-709B-402C-899C-3148A1CF3479"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""706208b3024200fd0a39ad499808dd0d98d74065"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P31"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P31"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 5741069,
                                                    ""id"": ""Q5741069""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$E0FA10C2-4C55-493C-9912-17C4CAB78C8E"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P18"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P18"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana around 1992.jpg"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""commonsMedia""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""q11649$1C5753D3-98BC-485A-9A14-103057343397"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P910"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P910"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 7434182,
                                                    ""id"": ""Q7434182""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$3975766d-4260-4522-8e2d-c9a4f18f72e2"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P740"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P740"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 233808,
                                                    ""id"": ""Q233808""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$a4f9d270-41ce-3907-5f31-b42f250efadd"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P136"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P136"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 11366,
                                                    ""id"": ""Q11366""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$FDFCA448-E553-4158-A0BA-C1466482D0A9"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""ad3457df0dd60bc5d77c988e7ebbdab821bd5a51"",
                                                ""snaks"": {
                                                    ""P854"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P854"",
                                                            ""datavalue"": {
                                                                ""value"": ""https://www.britannica.com/topic/Nirvana-band"",
                                                                ""type"": ""string""
                                                            },
                                                            ""datatype"": ""url""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P854""
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P136"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 11365,
                                                    ""id"": ""Q11365""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$7707A145-498F-4132-9FF8-06573D836D40"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""f83bc90d45006ac6ac6cc404ad221aa82efa4fe7"",
                                                ""snaks"": {
                                                    ""P854"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P854"",
                                                            ""datavalue"": {
                                                                ""value"": ""http://www.popmatters.com/pm/feature/115413-nirvana-bleach/"",
                                                                ""type"": ""string""
                                                            },
                                                            ""datatype"": ""url""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P854""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P214"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P214"",
                                            ""datavalue"": {
                                                ""value"": ""152633152"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$51CDB645-1ACD-4E63-A48C-42B9E4947F4C"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""9a24f7c0208b05d6be97077d855671d1dfdbc0dd"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 48183,
                                                                    ""id"": ""Q48183""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P646"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P646"",
                                            ""datavalue"": {
                                                ""value"": ""/m/0b1zz"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$FDDF735A-97F2-40C1-BAAD-1EA03A1DDD09"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""2b00cb481cddcac7623114367489b5c194901c4a"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 15241312,
                                                                    ""id"": ""Q15241312""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P577"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P577"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2013-10-28T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P577""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P269"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P269"",
                                            ""datavalue"": {
                                                ""value"": ""033595739"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$8F87CED8-3666-42DB-B348-9F3B779F13AF"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""63309730314f4c20bf6b1008fe8ffd2b155272b3"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 54919,
                                                                    ""id"": ""Q54919""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P166"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 179191,
                                                    ""id"": ""Q179191""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""049bd876bdfea826eaceb6f6056701b74c6d3f08"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+2014-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$825c0619-462d-b145-69b2-9bdf05f7ef0d"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 595693,
                                                    ""id"": ""Q595693""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$9CA31DE8-1182-4CBC-8BBC-24C184178C90"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 821010,
                                                    ""id"": ""Q821010""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$493FF2FB-4F98-4B23-BCAB-BB2EF7C3B4E4"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 16985322,
                                                    ""id"": ""Q16985322""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""90c1ec53fb2353d85282cca17898f95b43cc4d63"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1993-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$A1BB0250-A9F2-4B25-8891-B97696A1F0E9"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 821010,
                                                    ""id"": ""Q821010""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""90c1ec53fb2353d85282cca17898f95b43cc4d63"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1993-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$22173065-C022-4F02-B75A-2A47C97228ED"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 917698,
                                                    ""id"": ""Q917698""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$B2957340-3BCD-464B-B9E8-6200B4F90279"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 821010,
                                                    ""id"": ""Q821010""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$77D4A4B3-9EC1-448B-90A9-4C5C096C6B38"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P166"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1542129,
                                                    ""id"": ""Q1542129""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""c09061faa6b6430b874257ac50be630b716a5d4a"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1995-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ],
                                            ""P1686"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P1686"",
                                                    ""hash"": ""f5664cb75a5895b01e8f354aef3c79be7c19b731"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""entity-type"": ""item"",
                                                            ""numeric-id"": 212326,
                                                            ""id"": ""Q212326""
                                                        },
                                                        ""type"": ""wikibase-entityid""
                                                    },
                                                    ""datatype"": ""wikibase-item""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585"",
                                            ""P1686""
                                        ],
                                        ""id"": ""Q11649$AC9F6463-3726-4869-829B-46747F4E32C4"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1286"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1286"",
                                            ""datavalue"": {
                                                ""value"": ""02000000400"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$56C52737-61F0-42DA-94B2-F16E00A5C6D1"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1728"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1728"",
                                            ""datavalue"": {
                                                ""value"": ""mn0000357406"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$DBE29FA9-B63F-4C55-BB78-42F794520EBD"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""d770a038a4ab6024956c3929bb0a220e1a7c08f0"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2015-07-25T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P1953"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1953"",
                                            ""datavalue"": {
                                                ""value"": ""125246"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$FAC517CB-686E-44B3-AF1B-4432512586F8"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""d770a038a4ab6024956c3929bb0a220e1a7c08f0"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2015-07-25T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P1902"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1902"",
                                            ""datavalue"": {
                                                ""value"": ""6olE6TJLqED3rqDCT0FyPh"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$1D31405F-C432-40BA-B787-CA4971CE74B0"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""7baabcb471032f84aab649b1694ccc0c3ecde53a"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2015-08-24T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P345"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P345"",
                                            ""datavalue"": {
                                                ""value"": ""nm1110321"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$9A6DE421-C4D3-498D-BC7B-E9135E4B3062"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""7baabcb471032f84aab649b1694ccc0c3ecde53a"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2015-08-24T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P2002"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P2002"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P3744"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P3744"",
                                                    ""hash"": ""5b0b79856b05382cc362a6af4a399f54d7ac254f"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""amount"": ""+935629"",
                                                            ""unit"": ""1""
                                                        },
                                                        ""type"": ""quantity""
                                                    },
                                                    ""datatype"": ""quantity""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P3744""
                                        ],
                                        ""id"": ""Q11649$BBE3590D-D283-4D7E-8FCF-C9D7DF07974E"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""7baabcb471032f84aab649b1694ccc0c3ecde53a"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2015-08-24T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            },
                                            {
                                                ""hash"": ""830e0d3d65e6b91e769889d7d1177706b5c1e0ca"",
                                                ""snaks"": {
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2018-05-10T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P856"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P856"",
                                            ""datavalue"": {
                                                ""value"": ""http://www.nirvana.com"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""url""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$989C9273-37C0-431B-A135-DC590C98357E"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""1f1ad4e8b24154f402e1afe4c76421acd00932f9"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 175482,
                                                                    ""id"": ""Q175482""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P571"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P571"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""time"": ""+1987-00-00T00:00:00Z"",
                                                    ""timezone"": 0,
                                                    ""before"": 0,
                                                    ""after"": 0,
                                                    ""precision"": 9,
                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                },
                                                ""type"": ""time""
                                            },
                                            ""datatype"": ""time""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$C1EB8E06-32A3-4797-903A-EDBC7FFFAACC"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""fa278ebfc458360e5aed63d5058cca83c46134f1"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 328,
                                                                    ""id"": ""Q328""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P2013"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P2013"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$A88807F0-9C6E-4F95-B21D-C729A032A250"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1417"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1417"",
                                            ""datavalue"": {
                                                ""value"": ""topic/Nirvana-band"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$467D5B13-D02B-44AB-AE0A-B62CB625B76C"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P2373"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P2373"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$9156A4CE-8BD8-444D-B389-AFA3AB90D130"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""7fd6c920e6de82f011740209b8f75acb3150a795"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 14005,
                                                                    ""id"": ""Q14005""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2016-04-28T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P737"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P737"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 188464,
                                                    ""id"": ""Q188464""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$694C1BE0-985A-4152-9231-6CC75692E673"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P737"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 188626,
                                                    ""id"": ""Q188626""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$18143cf9-492c-5180-2be3-d283b6245abf"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P737"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 208767,
                                                    ""id"": ""Q208767""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$2234c3fe-487f-fa28-eae1-7111974c2e64"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P737"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 2331,
                                                    ""id"": ""Q2331""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$9d34e25e-4996-17bd-6b34-15297cf2f5a3"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P737"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 47670,
                                                    ""id"": ""Q47670""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$dfdb0862-4c18-59c2-f19b-3e84333c87f1"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P264"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P264"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 2311775,
                                                    ""id"": ""Q2311775""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$234EAB53-0048-401D-80DB-19BA58FB6424"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P264"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 212699,
                                                    ""id"": ""Q212699""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$603924C5-938E-4A66-B61D-62CF370B0903"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P264"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 778673,
                                                    ""id"": ""Q778673""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$09F06174-FD29-4A1C-AEBC-B3F8B24940E3"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1411"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1542129,
                                                    ""id"": ""Q1542129""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""91e44407bbaf560304e82582230228a3da9501d8"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1991-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ],
                                            ""P1686"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P1686"",
                                                    ""hash"": ""47196dd59e7a667281b50354cde1cbec039eb6a7"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""entity-type"": ""item"",
                                                            ""numeric-id"": 17444,
                                                            ""id"": ""Q17444""
                                                        },
                                                        ""type"": ""wikibase-entityid""
                                                    },
                                                    ""datatype"": ""wikibase-item""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585"",
                                            ""P1686""
                                        ],
                                        ""id"": ""Q11649$1B6741E7-B8F1-4F8A-AC15-246C7FDD2A71"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 821010,
                                                    ""id"": ""Q821010""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$A087FADE-134C-4EAF-9ACD-3A0EB00529DE"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 282636,
                                                    ""id"": ""Q282636""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ],
                                            ""P1686"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P1686"",
                                                    ""hash"": ""56dd0f9afdc5628f463ffc0e88cea50fa6de625d"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""entity-type"": ""item"",
                                                            ""numeric-id"": 485907,
                                                            ""id"": ""Q485907""
                                                        },
                                                        ""type"": ""wikibase-entityid""
                                                    },
                                                    ""datatype"": ""wikibase-item""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585"",
                                            ""P1686""
                                        ],
                                        ""id"": ""Q11649$7834DFB4-A7C5-4C1E-88F1-A97CF82A3DB3"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 595693,
                                                    ""id"": ""Q595693""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$927D2954-6C35-4A43-88E4-29E06BB73E68"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1065651,
                                                    ""id"": ""Q1065651""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$03D30450-06F8-4F72-82DB-996F643C720A"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 667066,
                                                    ""id"": ""Q667066""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""4552704ecdc9a6e9719c3d80f8b8c3dbfa5c063e"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1992-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$CC4566C8-90B3-459C-998A-106F3302F19C"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 16985322,
                                                    ""id"": ""Q16985322""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""90c1ec53fb2353d85282cca17898f95b43cc4d63"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1993-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$4A9A5D8B-838E-4BDC-9AD3-EE5DA611D5DD"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1542129,
                                                    ""id"": ""Q1542129""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""90c1ec53fb2353d85282cca17898f95b43cc4d63"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1993-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ],
                                            ""P1686"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P1686"",
                                                    ""hash"": ""d05f9ed943aba56c97aa01e676856dec11d23130"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""entity-type"": ""item"",
                                                            ""numeric-id"": 222001,
                                                            ""id"": ""Q222001""
                                                        },
                                                        ""type"": ""wikibase-entityid""
                                                    },
                                                    ""datatype"": ""wikibase-item""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585"",
                                            ""P1686""
                                        ],
                                        ""id"": ""Q11649$CF34882A-D9C0-4F28-AB66-5802D6DFCD20"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 821010,
                                                    ""id"": ""Q821010""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""90c1ec53fb2353d85282cca17898f95b43cc4d63"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1993-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$1A17621C-50B6-417B-81B7-8992A77E03A4"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1542205,
                                                    ""id"": ""Q1542205""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$C85B4EE6-D996-4AD8-9C23-A207E7B27E68"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 917698,
                                                    ""id"": ""Q917698""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$0C6BDC23-CA30-4936-95CA-B45DD0A91197"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 821010,
                                                    ""id"": ""Q821010""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$EA66A025-6247-4844-81EC-2C497284B628"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 667066,
                                                    ""id"": ""Q667066""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$FB20B815-E5E5-4495-B1D1-44A249DF6261"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 615682,
                                                    ""id"": ""Q615682""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$7E804B62-32D6-4E7E-9C34-C379570577CB"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1065651,
                                                    ""id"": ""Q1065651""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""b7e7105ce8e4bb89a98287b17c8b17d5ac4b5e57"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1994-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585""
                                        ],
                                        ""id"": ""Q11649$EFD631B9-8462-4EE6-9895-AEAE1EB56778"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1411"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 1542129,
                                                    ""id"": ""Q1542129""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P585"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P585"",
                                                    ""hash"": ""c09061faa6b6430b874257ac50be630b716a5d4a"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""time"": ""+1995-00-00T00:00:00Z"",
                                                            ""timezone"": 0,
                                                            ""before"": 0,
                                                            ""after"": 0,
                                                            ""precision"": 9,
                                                            ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                        },
                                                        ""type"": ""time""
                                                    },
                                                    ""datatype"": ""time""
                                                }
                                            ],
                                            ""P1686"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P1686"",
                                                    ""hash"": ""f5664cb75a5895b01e8f354aef3c79be7c19b731"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""entity-type"": ""item"",
                                                            ""numeric-id"": 212326,
                                                            ""id"": ""Q212326""
                                                        },
                                                        ""type"": ""wikibase-entityid""
                                                    },
                                                    ""datatype"": ""wikibase-item""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P585"",
                                            ""P1686""
                                        ],
                                        ""id"": ""Q11649$D7A62BF0-3AC3-4ED2-AE87-8E4574244185"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1553"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1553"",
                                            ""datavalue"": {
                                                ""value"": ""9262"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$255848fe-4670-5a48-fff3-c8c7f81dee1f"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1424"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1424"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 6605689,
                                                    ""id"": ""Q6605689""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$d799292e-49af-0b46-7216-e354741c65de"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P950"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P950"",
                                            ""datavalue"": {
                                                ""value"": ""XX144348"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$702D8622-45DC-44FF-884B-9CD7DD412167"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P495"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P495"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 30,
                                                    ""id"": ""Q30""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$CFF89CD7-62D7-4970-B644-13BBF8775248"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P3192"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3192"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$D0BE04DA-7BB0-40A9-ACB0-6391F6E34957"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""825d3115c1a2cc09a43c53ddcdd0634c4dd7d8f9"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 202472,
                                                                    ""id"": ""Q202472""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P3221"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3221"",
                                            ""datavalue"": {
                                                ""value"": ""organization/nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$5298C35C-6F9D-4CAF-A8E0-A49F37DDBABA"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P3106"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3106"",
                                            ""datavalue"": {
                                                ""value"": ""music/nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$8D2001C8-79DE-493D-AB1B-CFF173094EC7"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""3f1950b5890dcbc0f146c846392d8faf4a80f6a5"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 11148,
                                                                    ""id"": ""Q11148""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P3733"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3733"",
                                            ""datavalue"": {
                                                ""value"": ""195"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$E451B4CE-D2B2-40AC-A967-49EC2D9A6EB5"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""ed8bd3f9343e9a35e58b67284f26a67859f1808d"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 199913,
                                                                    ""id"": ""Q199913""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P154"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P154"",
                                            ""datavalue"": {
                                                ""value"": ""NirvanaLogo.svg"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""commonsMedia""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$c359319e-49cf-0374-73c1-332dd6498be5"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""288ab581e7d2d02995a26dfa8b091d96e78457fc"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 206855,
                                                                    ""id"": ""Q206855""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P3417"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3417"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana-band"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$7501c72e-456d-bfc3-5de0-ffeb2147c7fa"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P3265"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3265"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$EB1A4567-2C34-4321-AA9B-C7D8863E2D82"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""ed8bd3f9343e9a35e58b67284f26a67859f1808d"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 199913,
                                                                    ""id"": ""Q199913""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P3017"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3017"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$cc494412-4d46-9d96-1d7e-a6dcf82513d4"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P3509"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3509"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$68FFC6CA-B8BC-4A29-8AB7-127C05C871FB"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P527"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P527"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 8446,
                                                    ""id"": ""Q8446""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$217385f2-4327-74cc-9189-23e39914ab86"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P527"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 428798,
                                                    ""id"": ""Q428798""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$63a2f622-4d72-c412-0084-75ed181c1f85"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P527"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 12006,
                                                    ""id"": ""Q12006""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$04fc4184-4e90-478c-2cbb-9c6dd73daf32"",
                                        ""rank"": ""normal""
                                    },
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P527"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 353973,
                                                    ""id"": ""Q353973""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$0c994057-4a7e-8204-2354-bc154980b619"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P3162"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3162"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$e0ff4023-4a94-b6f6-4bf4-c95b0e885194"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P4342"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P4342"",
                                            ""datavalue"": {
                                                ""value"": ""Nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""qualifiers"": {
                                            ""P4390"": [
                                                {
                                                    ""snaktype"": ""value"",
                                                    ""property"": ""P4390"",
                                                    ""hash"": ""1a4df62914ea9afca349bd5fb5d8efd5832d83fa"",
                                                    ""datavalue"": {
                                                        ""value"": {
                                                            ""entity-type"": ""item"",
                                                            ""numeric-id"": 39893449,
                                                            ""id"": ""Q39893449""
                                                        },
                                                        ""type"": ""wikibase-entityid""
                                                    },
                                                    ""datatype"": ""wikibase-item""
                                                }
                                            ]
                                        },
                                        ""qualifiers-order"": [
                                            ""P4390""
                                        ],
                                        ""id"": ""Q11649$2A3BA581-15D7-451D-AB58-FD1F2A902DFA"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P5356"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P5356"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$321CB67A-F7DF-49F5-8BAC-6A33FDD2EFB2"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""9a24f7c0208b05d6be97077d855671d1dfdbc0dd"",
                                                ""snaks"": {
                                                    ""P143"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P143"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 48183,
                                                                    ""id"": ""Q48183""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P143""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P213"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P213"",
                                            ""datavalue"": {
                                                ""value"": ""0000 0001 2348 7390"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$8EBA4B0A-183D-475D-BDEC-B9819E748A7A"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""e159b8548fdca420eb9aef93f7251d2eebfb0abe"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 19938912,
                                                                    ""id"": ""Q19938912""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2015-07-13T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P1015"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1015"",
                                            ""datavalue"": {
                                                ""value"": ""10012507"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$9DE43CDA-CE58-4461-8BD7-5E4079574222"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1296"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1296"",
                                            ""datavalue"": {
                                                ""value"": ""0247912"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$E553241A-138A-4472-AE1C-CFAAAC06D1FF"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1315"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1315"",
                                            ""datavalue"": {
                                                ""value"": ""1179730"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$21C14070-D6F5-450C-9488-4EB6FE3145C5"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P1711"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P1711"",
                                            ""datavalue"": {
                                                ""value"": ""207922"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$527E15B8-E3C6-4124-B897-68C392007AC9"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P3222"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P3222"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana-(2)"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$BEDA96FF-A2D8-41B6-BB98-6F58FF94DF7A"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P691"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P691"",
                                            ""datavalue"": {
                                                ""value"": ""kn20030215074"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$72664e34-4f42-8f76-e986-96a9a250a9ef"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P268"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P268"",
                                            ""datavalue"": {
                                                ""value"": ""13944446b"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$A4B09177-76CD-43AD-AA8E-33CFC5246973"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""ed27b260511f51ddad849e3f6dedac6ea83bded2"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 54919,
                                                                    ""id"": ""Q54919""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P214"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P214"",
                                                            ""datavalue"": {
                                                                ""value"": ""152633152"",
                                                                ""type"": ""string""
                                                            },
                                                            ""datatype"": ""external-id""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2018-10-07T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P214"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P6365"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P6365"",
                                            ""datavalue"": {
                                                ""value"": {
                                                    ""entity-type"": ""item"",
                                                    ""numeric-id"": 6357124,
                                                    ""id"": ""Q6357124""
                                                },
                                                ""type"": ""wikibase-entityid""
                                            },
                                            ""datatype"": ""wikibase-item""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$6421DB05-72C6-4AD1-AE47-C55832E04690"",
                                        ""rank"": ""normal""
                                    }
                                ],
                                ""P6351"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P6351"",
                                            ""datavalue"": {
                                                ""value"": ""12712"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$79018CA1-40E4-422B-9231-C5DBA1B8D347"",
                                        ""rank"": ""normal"",
                                        ""references"": [
                                            {
                                                ""hash"": ""b8369f83a2116f0524ae4581f9a774592e765242"",
                                                ""snaks"": {
                                                    ""P248"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P248"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""entity-type"": ""item"",
                                                                    ""numeric-id"": 3419343,
                                                                    ""id"": ""Q3419343""
                                                                },
                                                                ""type"": ""wikibase-entityid""
                                                            },
                                                            ""datatype"": ""wikibase-item""
                                                        }
                                                    ],
                                                    ""P813"": [
                                                        {
                                                            ""snaktype"": ""value"",
                                                            ""property"": ""P813"",
                                                            ""datavalue"": {
                                                                ""value"": {
                                                                    ""time"": ""+2019-01-26T00:00:00Z"",
                                                                    ""timezone"": 0,
                                                                    ""before"": 0,
                                                                    ""after"": 0,
                                                                    ""precision"": 11,
                                                                    ""calendarmodel"": ""http://www.wikidata.org/entity/Q1985727""
                                                                },
                                                                ""type"": ""time""
                                                            },
                                                            ""datatype"": ""time""
                                                        }
                                                    ]
                                                },
                                                ""snaks-order"": [
                                                    ""P248"",
                                                    ""P813""
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                ""P4208"": [
                                    {
                                        ""mainsnak"": {
                                            ""snaktype"": ""value"",
                                            ""property"": ""P4208"",
                                            ""datavalue"": {
                                                ""value"": ""nirvana"",
                                                ""type"": ""string""
                                            },
                                            ""datatype"": ""external-id""
                                        },
                                        ""type"": ""statement"",
                                        ""id"": ""Q11649$F560B6E5-F495-4653-B73C-6A887EC01E32"",
                                        ""rank"": ""normal""
                                    }
                                ]
                            },
                            ""sitelinks"": {
                                ""angwiki"": {
                                    ""site"": ""angwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://ang.wikipedia.org/wiki/Nirvana""
                                },
                                ""anwiki"": {
                                    ""site"": ""anwiki"",
                                    ""title"": ""Nirvana (grupo)"",
                                    ""badges"": [],
                                    ""url"": ""https://an.wikipedia.org/wiki/Nirvana_(grupo)""
                                },
                                ""arwiki"": {
                                    ""site"": ""arwiki"",
                                    ""title"": ""نيرفانا (فرقة موسيقية)"",
                                    ""badges"": [],
                                    ""url"": ""https://ar.wikipedia.org/wiki/%D9%86%D9%8A%D8%B1%D9%81%D8%A7%D9%86%D8%A7_(%D9%81%D8%B1%D9%82%D8%A9_%D9%85%D9%88%D8%B3%D9%8A%D9%82%D9%8A%D8%A9)""
                                },
                                ""astwiki"": {
                                    ""site"": ""astwiki"",
                                    ""title"": ""Nirvana (grupu)"",
                                    ""badges"": [],
                                    ""url"": ""https://ast.wikipedia.org/wiki/Nirvana_(grupu)""
                                },
                                ""azbwiki"": {
                                    ""site"": ""azbwiki"",
                                    ""title"": ""نیروانا (موزیک قروپو)"",
                                    ""badges"": [],
                                    ""url"": ""https://azb.wikipedia.org/wiki/%D9%86%DB%8C%D8%B1%D9%88%D8%A7%D9%86%D8%A7_(%D9%85%D9%88%D8%B2%DB%8C%DA%A9_%D9%82%D8%B1%D9%88%D9%BE%D9%88)""
                                },
                                ""azwiki"": {
                                    ""site"": ""azwiki"",
                                    ""title"": ""Nirvana (qrup)"",
                                    ""badges"": [],
                                    ""url"": ""https://az.wikipedia.org/wiki/Nirvana_(qrup)""
                                },
                                ""barwiki"": {
                                    ""site"": ""barwiki"",
                                    ""title"": ""Nirvana (Band)"",
                                    ""badges"": [],
                                    ""url"": ""https://bar.wikipedia.org/wiki/Nirvana_(Band)""
                                },
                                ""be_x_oldwiki"": {
                                    ""site"": ""be_x_oldwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://be-tarask.wikipedia.org/wiki/Nirvana""
                                },
                                ""bewiki"": {
                                    ""site"": ""bewiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://be.wikipedia.org/wiki/Nirvana""
                                },
                                ""bgwiki"": {
                                    ""site"": ""bgwiki"",
                                    ""title"": ""Нирвана (група)"",
                                    ""badges"": [],
                                    ""url"": ""https://bg.wikipedia.org/wiki/%D0%9D%D0%B8%D1%80%D0%B2%D0%B0%D0%BD%D0%B0_(%D0%B3%D1%80%D1%83%D0%BF%D0%B0)""
                                },
                                ""bnwiki"": {
                                    ""site"": ""bnwiki"",
                                    ""title"": ""নিরভানা"",
                                    ""badges"": [],
                                    ""url"": ""https://bn.wikipedia.org/wiki/%E0%A6%A8%E0%A6%BF%E0%A6%B0%E0%A6%AD%E0%A6%BE%E0%A6%A8%E0%A6%BE""
                                },
                                ""brwiki"": {
                                    ""site"": ""brwiki"",
                                    ""title"": ""Nirvana (strollad sonerezh)"",
                                    ""badges"": [],
                                    ""url"": ""https://br.wikipedia.org/wiki/Nirvana_(strollad_sonerezh)""
                                },
                                ""bswiki"": {
                                    ""site"": ""bswiki"",
                                    ""title"": ""Nirvana (grupa)"",
                                    ""badges"": [
                                        ""Q17437798""
                                    ],
                                    ""url"": ""https://bs.wikipedia.org/wiki/Nirvana_(grupa)""
                                },
                                ""cawiki"": {
                                    ""site"": ""cawiki"",
                                    ""title"": ""Nirvana (grup de música)"",
                                    ""badges"": [],
                                    ""url"": ""https://ca.wikipedia.org/wiki/Nirvana_(grup_de_m%C3%BAsica)""
                                },
                                ""ckbwiki"": {
                                    ""site"": ""ckbwiki"",
                                    ""title"": ""نیرڤانا (گرووپ)"",
                                    ""badges"": [],
                                    ""url"": ""https://ckb.wikipedia.org/wiki/%D9%86%DB%8C%D8%B1%DA%A4%D8%A7%D9%86%D8%A7_(%DA%AF%D8%B1%D9%88%D9%88%D9%BE)""
                                },
                                ""cowiki"": {
                                    ""site"": ""cowiki"",
                                    ""title"": ""Nirvana (gruppu)"",
                                    ""badges"": [],
                                    ""url"": ""https://co.wikipedia.org/wiki/Nirvana_(gruppu)""
                                },
                                ""csbwiki"": {
                                    ""site"": ""csbwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://csb.wikipedia.org/wiki/Nirvana""
                                },
                                ""cswiki"": {
                                    ""site"": ""cswiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [
                                        ""Q17437796""
                                    ],
                                    ""url"": ""https://cs.wikipedia.org/wiki/Nirvana""
                                },
                                ""cswikiquote"": {
                                    ""site"": ""cswikiquote"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://cs.wikiquote.org/wiki/Nirvana""
                                },
                                ""cywiki"": {
                                    ""site"": ""cywiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://cy.wikipedia.org/wiki/Nirvana""
                                },
                                ""dawiki"": {
                                    ""site"": ""dawiki"",
                                    ""title"": ""Nirvana (band)"",
                                    ""badges"": [],
                                    ""url"": ""https://da.wikipedia.org/wiki/Nirvana_(band)""
                                },
                                ""dewiki"": {
                                    ""site"": ""dewiki"",
                                    ""title"": ""Nirvana (US-amerikanische Band)"",
                                    ""badges"": [],
                                    ""url"": ""https://de.wikipedia.org/wiki/Nirvana_(US-amerikanische_Band)""
                                },
                                ""diqwiki"": {
                                    ""site"": ""diqwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://diq.wikipedia.org/wiki/Nirvana""
                                },
                                ""elwiki"": {
                                    ""site"": ""elwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://el.wikipedia.org/wiki/Nirvana""
                                },
                                ""emlwiki"": {
                                    ""site"": ""emlwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://eml.wikipedia.org/wiki/Nirvana""
                                },
                                ""enwiki"": {
                                    ""site"": ""enwiki"",
                                    ""title"": ""Nirvana (band)"",
                                    ""badges"": [
                                        ""Q17437796""
                                    ],
                                    ""url"": ""https://en.wikipedia.org/wiki/Nirvana_(band)""
                                },
                                ""enwikiquote"": {
                                    ""site"": ""enwikiquote"",
                                    ""title"": ""Nirvana (band)"",
                                    ""badges"": [],
                                    ""url"": ""https://en.wikiquote.org/wiki/Nirvana_(band)""
                                },
                                ""eowiki"": {
                                    ""site"": ""eowiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://eo.wikipedia.org/wiki/Nirvana""
                                },
                                ""eswiki"": {
                                    ""site"": ""eswiki"",
                                    ""title"": ""Nirvana (banda)"",
                                    ""badges"": [
                                        ""Q17437796""
                                    ],
                                    ""url"": ""https://es.wikipedia.org/wiki/Nirvana_(banda)""
                                },
                                ""eswikiquote"": {
                                    ""site"": ""eswikiquote"",
                                    ""title"": ""Nirvana (banda)"",
                                    ""badges"": [],
                                    ""url"": ""https://es.wikiquote.org/wiki/Nirvana_(banda)""
                                },
                                ""etwiki"": {
                                    ""site"": ""etwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://et.wikipedia.org/wiki/Nirvana""
                                },
                                ""euwiki"": {
                                    ""site"": ""euwiki"",
                                    ""title"": ""Nirvana (musika taldea)"",
                                    ""badges"": [],
                                    ""url"": ""https://eu.wikipedia.org/wiki/Nirvana_(musika_taldea)""
                                },
                                ""extwiki"": {
                                    ""site"": ""extwiki"",
                                    ""title"": ""Nirvana (banda)"",
                                    ""badges"": [],
                                    ""url"": ""https://ext.wikipedia.org/wiki/Nirvana_(banda)""
                                },
                                ""fawiki"": {
                                    ""site"": ""fawiki"",
                                    ""title"": ""نیروانا (گروه موسیقی)"",
                                    ""badges"": [],
                                    ""url"": ""https://fa.wikipedia.org/wiki/%D9%86%DB%8C%D8%B1%D9%88%D8%A7%D9%86%D8%A7_(%DA%AF%D8%B1%D9%88%D9%87_%D9%85%D9%88%D8%B3%DB%8C%D9%82%DB%8C)""
                                },
                                ""fiwiki"": {
                                    ""site"": ""fiwiki"",
                                    ""title"": ""Nirvana (yhtye)"",
                                    ""badges"": [],
                                    ""url"": ""https://fi.wikipedia.org/wiki/Nirvana_(yhtye)""
                                },
                                ""frwiki"": {
                                    ""site"": ""frwiki"",
                                    ""title"": ""Nirvana (groupe)"",
                                    ""badges"": [
                                        ""Q17437796""
                                    ],
                                    ""url"": ""https://fr.wikipedia.org/wiki/Nirvana_(groupe)""
                                },
                                ""furwiki"": {
                                    ""site"": ""furwiki"",
                                    ""title"": ""Nirvana (clape musicâl)"",
                                    ""badges"": [],
                                    ""url"": ""https://fur.wikipedia.org/wiki/Nirvana_(clape_music%C3%A2l)""
                                },
                                ""gawiki"": {
                                    ""site"": ""gawiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://ga.wikipedia.org/wiki/Nirvana""
                                },
                                ""glwiki"": {
                                    ""site"": ""glwiki"",
                                    ""title"": ""Nirvana (grupo musical)"",
                                    ""badges"": [],
                                    ""url"": ""https://gl.wikipedia.org/wiki/Nirvana_(grupo_musical)""
                                },
                                ""gotwiki"": {
                                    ""site"": ""gotwiki"",
                                    ""title"": ""𐌽𐌹𐍂𐌱𐌰𐌽𐌰"",
                                    ""badges"": [],
                                    ""url"": ""https://got.wikipedia.org/wiki/%F0%90%8C%BD%F0%90%8C%B9%F0%90%8D%82%F0%90%8C%B1%F0%90%8C%B0%F0%90%8C%BD%F0%90%8C%B0""
                                },
                                ""hewiki"": {
                                    ""site"": ""hewiki"",
                                    ""title"": ""נירוונה (להקה)"",
                                    ""badges"": [],
                                    ""url"": ""https://he.wikipedia.org/wiki/%D7%A0%D7%99%D7%A8%D7%95%D7%95%D7%A0%D7%94_(%D7%9C%D7%94%D7%A7%D7%94)""
                                },
                                ""hewikiquote"": {
                                    ""site"": ""hewikiquote"",
                                    ""title"": ""נירוונה (להקה)"",
                                    ""badges"": [],
                                    ""url"": ""https://he.wikiquote.org/wiki/%D7%A0%D7%99%D7%A8%D7%95%D7%95%D7%A0%D7%94_(%D7%9C%D7%94%D7%A7%D7%94)""
                                },
                                ""hrwiki"": {
                                    ""site"": ""hrwiki"",
                                    ""title"": ""Nirvana (sastav)"",
                                    ""badges"": [],
                                    ""url"": ""https://hr.wikipedia.org/wiki/Nirvana_(sastav)""
                                },
                                ""hsbwiki"": {
                                    ""site"": ""hsbwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://hsb.wikipedia.org/wiki/Nirvana""
                                },
                                ""huwiki"": {
                                    ""site"": ""huwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://hu.wikipedia.org/wiki/Nirvana""
                                },
                                ""hywiki"": {
                                    ""site"": ""hywiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://hy.wikipedia.org/wiki/Nirvana""
                                },
                                ""idwiki"": {
                                    ""site"": ""idwiki"",
                                    ""title"": ""Nirvana (grup musik)"",
                                    ""badges"": [],
                                    ""url"": ""https://id.wikipedia.org/wiki/Nirvana_(grup_musik)""
                                },
                                ""iowiki"": {
                                    ""site"": ""iowiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://io.wikipedia.org/wiki/Nirvana""
                                },
                                ""iswiki"": {
                                    ""site"": ""iswiki"",
                                    ""title"": ""Nirvana (hljómsveit)"",
                                    ""badges"": [],
                                    ""url"": ""https://is.wikipedia.org/wiki/Nirvana_(hlj%C3%B3msveit)""
                                },
                                ""itwiki"": {
                                    ""site"": ""itwiki"",
                                    ""title"": ""Nirvana (gruppo musicale)"",
                                    ""badges"": [],
                                    ""url"": ""https://it.wikipedia.org/wiki/Nirvana_(gruppo_musicale)""
                                },
                                ""itwikiquote"": {
                                    ""site"": ""itwikiquote"",
                                    ""title"": ""Nirvana (gruppo musicale)"",
                                    ""badges"": [],
                                    ""url"": ""https://it.wikiquote.org/wiki/Nirvana_(gruppo_musicale)""
                                },
                                ""jawiki"": {
                                    ""site"": ""jawiki"",
                                    ""title"": ""ニルヴァーナ (アメリカ合衆国のバンド)"",
                                    ""badges"": [],
                                    ""url"": ""https://ja.wikipedia.org/wiki/%E3%83%8B%E3%83%AB%E3%83%B4%E3%82%A1%E3%83%BC%E3%83%8A_(%E3%82%A2%E3%83%A1%E3%83%AA%E3%82%AB%E5%90%88%E8%A1%86%E5%9B%BD%E3%81%AE%E3%83%90%E3%83%B3%E3%83%89)""
                                },
                                ""jvwiki"": {
                                    ""site"": ""jvwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://jv.wikipedia.org/wiki/Nirvana""
                                },
                                ""kawiki"": {
                                    ""site"": ""kawiki"",
                                    ""title"": ""ნირვანა (ჯგუფი)"",
                                    ""badges"": [],
                                    ""url"": ""https://ka.wikipedia.org/wiki/%E1%83%9C%E1%83%98%E1%83%A0%E1%83%95%E1%83%90%E1%83%9C%E1%83%90_(%E1%83%AF%E1%83%92%E1%83%A3%E1%83%A4%E1%83%98)""
                                },
                                ""kkwiki"": {
                                    ""site"": ""kkwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://kk.wikipedia.org/wiki/Nirvana""
                                },
                                ""kowiki"": {
                                    ""site"": ""kowiki"",
                                    ""title"": ""너바나"",
                                    ""badges"": [],
                                    ""url"": ""https://ko.wikipedia.org/wiki/%EB%84%88%EB%B0%94%EB%82%98""
                                },
                                ""kwwiki"": {
                                    ""site"": ""kwwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://kw.wikipedia.org/wiki/Nirvana""
                                },
                                ""kywiki"": {
                                    ""site"": ""kywiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://ky.wikipedia.org/wiki/Nirvana""
                                },
                                ""lawiki"": {
                                    ""site"": ""lawiki"",
                                    ""title"": ""Nirvana (grex)"",
                                    ""badges"": [],
                                    ""url"": ""https://la.wikipedia.org/wiki/Nirvana_(grex)""
                                },
                                ""ltwiki"": {
                                    ""site"": ""ltwiki"",
                                    ""title"": ""Nirvana (amerikiečių grupė)"",
                                    ""badges"": [],
                                    ""url"": ""https://lt.wikipedia.org/wiki/Nirvana_(amerikie%C4%8Di%C5%B3_grup%C4%97)""
                                },
                                ""lvwiki"": {
                                    ""site"": ""lvwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://lv.wikipedia.org/wiki/Nirvana""
                                },
                                ""mgwiki"": {
                                    ""site"": ""mgwiki"",
                                    ""title"": ""Nirvana (tarika)"",
                                    ""badges"": [],
                                    ""url"": ""https://mg.wikipedia.org/wiki/Nirvana_(tarika)""
                                },
                                ""mkwiki"": {
                                    ""site"": ""mkwiki"",
                                    ""title"": ""Нирвана (музичка група)"",
                                    ""badges"": [],
                                    ""url"": ""https://mk.wikipedia.org/wiki/%D0%9D%D0%B8%D1%80%D0%B2%D0%B0%D0%BD%D0%B0_(%D0%BC%D1%83%D0%B7%D0%B8%D1%87%D0%BA%D0%B0_%D0%B3%D1%80%D1%83%D0%BF%D0%B0)""
                                },
                                ""mlwiki"": {
                                    ""site"": ""mlwiki"",
                                    ""title"": ""നിർവാണ"",
                                    ""badges"": [],
                                    ""url"": ""https://ml.wikipedia.org/wiki/%E0%B4%A8%E0%B4%BF%E0%B5%BC%E0%B4%B5%E0%B4%BE%E0%B4%A3""
                                },
                                ""mnwiki"": {
                                    ""site"": ""mnwiki"",
                                    ""title"": ""Нирвана (хамтлаг)"",
                                    ""badges"": [],
                                    ""url"": ""https://mn.wikipedia.org/wiki/%D0%9D%D0%B8%D1%80%D0%B2%D0%B0%D0%BD%D0%B0_(%D1%85%D0%B0%D0%BC%D1%82%D0%BB%D0%B0%D0%B3)""
                                },
                                ""mswiki"": {
                                    ""site"": ""mswiki"",
                                    ""title"": ""Nirvana (kumpulan)"",
                                    ""badges"": [],
                                    ""url"": ""https://ms.wikipedia.org/wiki/Nirvana_(kumpulan)""
                                },
                                ""nahwiki"": {
                                    ""site"": ""nahwiki"",
                                    ""title"": ""Nirvana (tlacuīcaliztli)"",
                                    ""badges"": [],
                                    ""url"": ""https://nah.wikipedia.org/wiki/Nirvana_(tlacu%C4%ABcaliztli)""
                                },
                                ""ndswiki"": {
                                    ""site"": ""ndswiki"",
                                    ""title"": ""Nirvana (Grupp)"",
                                    ""badges"": [],
                                    ""url"": ""https://nds.wikipedia.org/wiki/Nirvana_(Grupp)""
                                },
                                ""newiki"": {
                                    ""site"": ""newiki"",
                                    ""title"": ""निर्भाना"",
                                    ""badges"": [],
                                    ""url"": ""https://ne.wikipedia.org/wiki/%E0%A4%A8%E0%A4%BF%E0%A4%B0%E0%A5%8D%E0%A4%AD%E0%A4%BE%E0%A4%A8%E0%A4%BE""
                                },
                                ""nlwiki"": {
                                    ""site"": ""nlwiki"",
                                    ""title"": ""Nirvana (Amerikaanse band)"",
                                    ""badges"": [],
                                    ""url"": ""https://nl.wikipedia.org/wiki/Nirvana_(Amerikaanse_band)""
                                },
                                ""nnwiki"": {
                                    ""site"": ""nnwiki"",
                                    ""title"": ""Musikkgruppa Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://nn.wikipedia.org/wiki/Musikkgruppa_Nirvana""
                                },
                                ""nowiki"": {
                                    ""site"": ""nowiki"",
                                    ""title"": ""Nirvana (band)"",
                                    ""badges"": [],
                                    ""url"": ""https://no.wikipedia.org/wiki/Nirvana_(band)""
                                },
                                ""ocwiki"": {
                                    ""site"": ""ocwiki"",
                                    ""title"": ""Nirvana (grop)"",
                                    ""badges"": [],
                                    ""url"": ""https://oc.wikipedia.org/wiki/Nirvana_(grop)""
                                },
                                ""pcdwiki"": {
                                    ""site"": ""pcdwiki"",
                                    ""title"": ""Nirvana (grope)"",
                                    ""badges"": [],
                                    ""url"": ""https://pcd.wikipedia.org/wiki/Nirvana_(grope)""
                                },
                                ""pdcwiki"": {
                                    ""site"": ""pdcwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://pdc.wikipedia.org/wiki/Nirvana""
                                },
                                ""plwiki"": {
                                    ""site"": ""plwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [
                                        ""Q17437798""
                                    ],
                                    ""url"": ""https://pl.wikipedia.org/wiki/Nirvana""
                                },
                                ""plwikiquote"": {
                                    ""site"": ""plwikiquote"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://pl.wikiquote.org/wiki/Nirvana""
                                },
                                ""pmswiki"": {
                                    ""site"": ""pmswiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://pms.wikipedia.org/wiki/Nirvana""
                                },
                                ""ptwiki"": {
                                    ""site"": ""ptwiki"",
                                    ""title"": ""Nirvana (banda)"",
                                    ""badges"": [
                                        ""Q17437796""
                                    ],
                                    ""url"": ""https://pt.wikipedia.org/wiki/Nirvana_(banda)""
                                },
                                ""rowiki"": {
                                    ""site"": ""rowiki"",
                                    ""title"": ""Nirvana (formație)"",
                                    ""badges"": [],
                                    ""url"": ""https://ro.wikipedia.org/wiki/Nirvana_(forma%C8%9Bie)""
                                },
                                ""ruwiki"": {
                                    ""site"": ""ruwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [
                                        ""Q17437798""
                                    ],
                                    ""url"": ""https://ru.wikipedia.org/wiki/Nirvana""
                                },
                                ""ruwikinews"": {
                                    ""site"": ""ruwikinews"",
                                    ""title"": ""Категория:Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://ru.wikinews.org/wiki/%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:Nirvana""
                                },
                                ""ruwikiquote"": {
                                    ""site"": ""ruwikiquote"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://ru.wikiquote.org/wiki/Nirvana""
                                },
                                ""scnwiki"": {
                                    ""site"": ""scnwiki"",
                                    ""title"": ""Nirvana (gruppu musicali)"",
                                    ""badges"": [],
                                    ""url"": ""https://scn.wikipedia.org/wiki/Nirvana_(gruppu_musicali)""
                                },
                                ""scowiki"": {
                                    ""site"": ""scowiki"",
                                    ""title"": ""Nirvana (baund)"",
                                    ""badges"": [],
                                    ""url"": ""https://sco.wikipedia.org/wiki/Nirvana_(baund)""
                                },
                                ""scwiki"": {
                                    ""site"": ""scwiki"",
                                    ""title"": ""Nirvana (grupu musicale)"",
                                    ""badges"": [],
                                    ""url"": ""https://sc.wikipedia.org/wiki/Nirvana_(grupu_musicale)""
                                },
                                ""shwiki"": {
                                    ""site"": ""shwiki"",
                                    ""title"": ""Nirvana (bend)"",
                                    ""badges"": [],
                                    ""url"": ""https://sh.wikipedia.org/wiki/Nirvana_(bend)""
                                },
                                ""simplewiki"": {
                                    ""site"": ""simplewiki"",
                                    ""title"": ""Nirvana (band)"",
                                    ""badges"": [],
                                    ""url"": ""https://simple.wikipedia.org/wiki/Nirvana_(band)""
                                },
                                ""skwiki"": {
                                    ""site"": ""skwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://sk.wikipedia.org/wiki/Nirvana""
                                },
                                ""slwiki"": {
                                    ""site"": ""slwiki"",
                                    ""title"": ""Nirvana (glasbena skupina)"",
                                    ""badges"": [],
                                    ""url"": ""https://sl.wikipedia.org/wiki/Nirvana_(glasbena_skupina)""
                                },
                                ""sqwiki"": {
                                    ""site"": ""sqwiki"",
                                    ""title"": ""Nirvana (grup muzikor)"",
                                    ""badges"": [],
                                    ""url"": ""https://sq.wikipedia.org/wiki/Nirvana_(grup_muzikor)""
                                },
                                ""srwiki"": {
                                    ""site"": ""srwiki"",
                                    ""title"": ""Нирвана (музичка група)"",
                                    ""badges"": [],
                                    ""url"": ""https://sr.wikipedia.org/wiki/%D0%9D%D0%B8%D1%80%D0%B2%D0%B0%D0%BD%D0%B0_(%D0%BC%D1%83%D0%B7%D0%B8%D1%87%D0%BA%D0%B0_%D0%B3%D1%80%D1%83%D0%BF%D0%B0)""
                                },
                                ""suwiki"": {
                                    ""site"": ""suwiki"",
                                    ""title"": ""Nirvana (grup musik)"",
                                    ""badges"": [],
                                    ""url"": ""https://su.wikipedia.org/wiki/Nirvana_(grup_musik)""
                                },
                                ""svwiki"": {
                                    ""site"": ""svwiki"",
                                    ""title"": ""Nirvana (musikgrupp)"",
                                    ""badges"": [
                                        ""Q17437796""
                                    ],
                                    ""url"": ""https://sv.wikipedia.org/wiki/Nirvana_(musikgrupp)""
                                },
                                ""szlwiki"": {
                                    ""site"": ""szlwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://szl.wikipedia.org/wiki/Nirvana""
                                },
                                ""thwiki"": {
                                    ""site"": ""thwiki"",
                                    ""title"": ""เนอร์วานา"",
                                    ""badges"": [],
                                    ""url"": ""https://th.wikipedia.org/wiki/%E0%B9%80%E0%B8%99%E0%B8%AD%E0%B8%A3%E0%B9%8C%E0%B8%A7%E0%B8%B2%E0%B8%99%E0%B8%B2""
                                },
                                ""tlwiki"": {
                                    ""site"": ""tlwiki"",
                                    ""title"": ""Nirvana (banda)"",
                                    ""badges"": [],
                                    ""url"": ""https://tl.wikipedia.org/wiki/Nirvana_(banda)""
                                },
                                ""trwiki"": {
                                    ""site"": ""trwiki"",
                                    ""title"": ""Nirvana (müzik grubu)"",
                                    ""badges"": [],
                                    ""url"": ""https://tr.wikipedia.org/wiki/Nirvana_(m%C3%BCzik_grubu)""
                                },
                                ""ttwiki"": {
                                    ""site"": ""ttwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://tt.wikipedia.org/wiki/Nirvana""
                                },
                                ""ukwiki"": {
                                    ""site"": ""ukwiki"",
                                    ""title"": ""Nirvana"",
                                    ""badges"": [],
                                    ""url"": ""https://uk.wikipedia.org/wiki/Nirvana""
                                },
                                ""uzwiki"": {
                                    ""site"": ""uzwiki"",
                                    ""title"": ""Nirvana (guruh)"",
                                    ""badges"": [
                                        ""Q17437798""
                                    ],
                                    ""url"": ""https://uz.wikipedia.org/wiki/Nirvana_(guruh)""
                                },
                                ""viwiki"": {
                                    ""site"": ""viwiki"",
                                    ""title"": ""Nirvana (ban nhạc)"",
                                    ""badges"": [],
                                    ""url"": ""https://vi.wikipedia.org/wiki/Nirvana_(ban_nh%E1%BA%A1c)""
                                },
                                ""warwiki"": {
                                    ""site"": ""warwiki"",
                                    ""title"": ""Nirvana (banda)"",
                                    ""badges"": [],
                                    ""url"": ""https://war.wikipedia.org/wiki/Nirvana_(banda)""
                                },
                                ""xmfwiki"": {
                                    ""site"": ""xmfwiki"",
                                    ""title"": ""ნირვანა (ბუნა)"",
                                    ""badges"": [],
                                    ""url"": ""https://xmf.wikipedia.org/wiki/%E1%83%9C%E1%83%98%E1%83%A0%E1%83%95%E1%83%90%E1%83%9C%E1%83%90_(%E1%83%91%E1%83%A3%E1%83%9C%E1%83%90)""
                                },
                                ""zh_yuewiki"": {
                                    ""site"": ""zh_yuewiki"",
                                    ""title"": ""涅槃樂隊"",
                                    ""badges"": [],
                                    ""url"": ""https://zh-yue.wikipedia.org/wiki/%E6%B6%85%E6%A7%83%E6%A8%82%E9%9A%8A""
                                },
                                ""zhwiki"": {
                                    ""site"": ""zhwiki"",
                                    ""title"": ""涅槃乐队"",
                                    ""badges"": [],
                                    ""url"": ""https://zh.wikipedia.org/wiki/%E6%B6%85%E6%A7%83%E4%B9%90%E9%98%9F""
                                }
                            }
                        }
                    }
                }";

            var message = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data, Encoding.UTF8, "application/json"),
            };
            var service = CreateService(message);

            // Act
            var result = await service.GetData("Q11649");

            // Assert
            Assert.Equal("Q11649", result.Id);
            Assert.Equal("Nirvana", result.Labels["en"].Value);
            Assert.Equal("Nirvana (band)", result.SiteLinks["enwiki"].Title);
        }

        private IWikiDataService CreateService(HttpResponseMessage message)
        {
            var handler = new Mock<HttpMessageHandler>();
            handler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>()).Returns(Task.FromResult(message));
            var client = new HttpClient(handler.Object);
            client.BaseAddress = new Uri("http://localhost/");

            return new WikiDataService(client);
        }
    }
}