using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using MusicBrainz.API.Models;
using Xunit;

namespace MusicBrainz.API.Tests.Controllers
{
    public class ArtistController_GetShould: IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> fixture;

        public ArtistController_GetShould(WebApplicationFactory<Startup> fixture)
        {
            this.fixture = fixture;
        }

        [Fact]
        public async Task ReturnNotFoundWhenIdIsUnknown()
        {
            // Arrange
            var client = fixture.CreateClient();
            
            // Act
            var response = await client.GetAsync("/api/artists/a");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task ReturnOkWhenIdIsValid()
        {
            // Arrange
            var client = fixture.CreateClient();
            
            // Act
            var response = await client.GetAsync("/api/artists/5b11f4ce-a62d-471e-81fc-a69a8278c7da");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task ReturnArtistModelWhenIdIsValid()
        {
            // Arrange
            var client = fixture.CreateClient();
            
            // Act
            var response = await client.GetAsync("/api/artists/5b11f4ce-a62d-471e-81fc-a69a8278c7da");
            var result = await response.Content.ReadAsAsync<ArtistModel>();
            var album = result.Albums.FirstOrDefault(x => x.Id == "ff9dec8b-3674-35a3-aa39-9f9ba3d30b71"); // Chose one album to test

            // Assert
            Assert.Equal("5b11f4ce-a62d-471e-81fc-a69a8278c7da", result.Mbid);
            Assert.Equal("Nirvana", result.Name);
            Assert.Equal("US", result.Country);
            Assert.Contains("Nirvana", result.Description);
            Assert.Equal("1988-01", result.StartedDate);
            Assert.Equal("1994-04-05", result.EndedDate);
            Assert.True(result.Ended);
            Assert.Equal(25, result.Albums.Count());

            Assert.NotNull(album);
            Assert.Equal("Twilight of the Gods", album.Title);
            Assert.Equal("1995", album.ReleaseDate);
            Assert.Equal("http://coverartarchive.org/release/fb27268a-b405-4ade-82ed-e02476987428/2222064701.jpg", album.Image.ToString());
        }
    }
}
