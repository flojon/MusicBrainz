FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY MusicBrainz.API/*.csproj ./MusicBrainz.API/
COPY MusicBrainz.API.Tests/*.csproj ./MusicBrainz.API.Tests/
RUN dotnet restore

# copy everything else and build app
COPY . ./
RUN dotnet build

# Run tests
FROM build as test
WORKDIR /app/MusicBrainz.API.Tests
ENTRYPOINT ["dotnet", "test", "--no-build", "--logger:trx"]

FROM build AS publish
WORKDIR /app/MusicBrainz.API
RUN dotnet publish -c Release -o out /p:PublishWithAspNetCoreTargetManifest="false"

FROM mcr.microsoft.com/dotnet/core/runtime:2.2 AS runtime
WORKDIR /app
COPY --from=publish /app/MusicBrainz.API/out ./
ENTRYPOINT ["dotnet", "MusicBrainz.API.dll"]
