using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public class WikipediaService : IWikipediaService
    {
        private readonly HttpClient client;

        public WikipediaService(HttpClient client)
        {
            this.client = client;
        }


        public async Task<QueryResult> GetExtract(string titles)
        {
            var url = $"?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles={titles}";

            var result = await client.GetAsync(url);
            if (result.StatusCode != HttpStatusCode.OK)
                return null;

            return await result.Content.ReadAsAsync<QueryResult>();
        }
    }
}