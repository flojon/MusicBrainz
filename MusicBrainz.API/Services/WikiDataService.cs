using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;
using Newtonsoft.Json.Linq;

namespace MusicBrainz.API.Services
{
    public class WikiDataService : IWikiDataService
    {
        private readonly HttpClient client;

        public WikiDataService(HttpClient client)
        {
            this.client = client;
        }


        public async Task<WikiData> GetData(string id)
        {
            var url = $"/wiki/Special:EntityData/{id}.json";

            var result = await client.GetAsync(url);
            if (result.StatusCode != HttpStatusCode.OK)
                return null;

            var data = await result.Content.ReadAsStringAsync();
            var wikiData = JObject.Parse(data);
            return wikiData["entities"][id].ToObject<WikiData>();
        }
    }
}