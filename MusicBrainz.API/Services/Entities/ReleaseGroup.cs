using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MusicBrainz.API.Services.Entities
{
    public class ReleaseGroup
    {
        public string Id { get; set; }
        public string Title { get; set; }

        [JsonProperty("primary-type")]
        public string PrimaryType { get; set; }

        [JsonProperty("primary-type-id")]
        public string PrimaryTypeId { get; set; }

        [JsonProperty("secondary-types")]
        public IEnumerable<string> SecondaryTypes { get; set; }

        [JsonProperty("secondary-type-ids")]
        public IEnumerable<string> SecondaryTypeIds { get; set; }

        [JsonProperty("first-release-date")]
        public string FirstReleaseDate { get; set; }

        public string Disambiguation { get; set; }
      }
}
