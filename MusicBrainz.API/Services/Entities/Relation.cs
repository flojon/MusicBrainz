using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MusicBrainz.API.Services.Entities
{
    public class Relation
    {
            public string Type { get; set; }

            [JsonProperty("type-id")]
            public string TypeId { get; set;}

            [JsonProperty("target-type")]
            public string TargetType { get; set; }

            public Url Url { get; set; }
            public string Direction { get; set; }
            public bool Ended { get; set; }
            public string Begin { get; set; }
            public string End { get; set; }
    }
}
