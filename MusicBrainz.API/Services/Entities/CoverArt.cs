using System;
using System.Collections.Generic;

namespace MusicBrainz.API.Services.Entities
{
    public class CoverArt
    {
        public Uri Release { get; set; }
        public IEnumerable<Image> Images { get; set; }
    }
}
