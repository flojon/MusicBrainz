using System.Collections;
using System.Collections.Generic;

namespace MusicBrainz.API.Services.Entities
{
    public class QueryResult
    {
        public string Batchcomplete { get; set; }
        public IDictionary<string, IDictionary<string, string>> Warnings { get; set; }

        public Query Query { get; set; }
    }
}