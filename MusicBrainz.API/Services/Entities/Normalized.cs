namespace MusicBrainz.API.Services.Entities
{
    public class Normalized
    {
        public string From { get; set; }
        public string To { get; set; }
    }
}