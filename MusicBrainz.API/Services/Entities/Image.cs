using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MusicBrainz.API.Services.Entities
{
    public class Image
    {
        public string Id { get; set; }

        [JsonProperty("image")]
        public Uri ImageUrl { get; set; }
        public IDictionary<string, Uri> Thumbnails { get; set; }
        public string Comment { get; set; }
        public bool Approved { get; set; }
        public bool Front { get; set; }
        public bool Back { get; set; }
        public IEnumerable<string> Types { get; set; }
        public int Edit { get; set; }
    }
}