namespace MusicBrainz.API.Services.Entities
{
    public class Url
    {
        public string Resource { get; set; }
        public string Id { get; set; }
    }
}
