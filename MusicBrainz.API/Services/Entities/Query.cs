using System.Collections.Generic;

namespace MusicBrainz.API.Services.Entities
{
    public class Query
    {
        public IEnumerable<Normalized> Normalized { get; set; }
        public IDictionary<string, Page> Pages { get; set; }
    }
}