using Newtonsoft.Json;

namespace MusicBrainz.API.Services.Entities
{
    public class Page
    {
        [JsonProperty("pageid")]
        public string PageId { get; set; }
        public int Ns { get; set; }
        public string Title { get; set; }
        public string Extract { get; set; }
    }
}