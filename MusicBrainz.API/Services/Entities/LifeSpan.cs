namespace MusicBrainz.API.Services.Entities
{
    public class LifeSpan
    {
        public bool Ended { get; set; }
        public string Begin { get; set; }
        public string End { get; set; }
    }
}
