using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MusicBrainz.API.Services.Entities
{
    public class Label
    {
        public string Language { get; set; }
        public string Value { get; set; }
    }

    public class SiteLink
    {
        public string Site { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }

    public class WikiData
    {
        public string Id { get; set; }
        public DateTime Modified { get; set; }

        public IDictionary<string, Label> Labels { get; set; }

        [JsonProperty("sitelinks")]
        public IDictionary<string, SiteLink> SiteLinks { get; set; }
    }
}
