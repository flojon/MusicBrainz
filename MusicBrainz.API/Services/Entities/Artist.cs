using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MusicBrainz.API.Services.Entities
{

    public class Artist
    {
        public string Id { get; set; }
        public string Name { get; set; }

        [JsonProperty("sort-name")]
        public string SortName { get; set; }

        public string Type { get; set; }

        [JsonProperty("life-span")]
        public LifeSpan LifeSpan { get; set; }

        public string Gender { get; set; }
        public string Country { get; set; }

        public IEnumerable<Relation> Relations { get; set; }

        [JsonProperty("release-groups")]
        public IEnumerable<ReleaseGroup> ReleaseGroups { get; set; }
    }
}
