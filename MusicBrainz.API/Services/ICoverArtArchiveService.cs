using System;
using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public interface ICoverArtArchiveService
    {
        Task<CoverArt> GetCoverArt(string id);
    }
}