using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public class CoverArtArchiveService: ICoverArtArchiveService
    {
        private readonly HttpClient client;

        public CoverArtArchiveService(HttpClient client)
        {
            this.client = client;
        }

        public async Task<CoverArt> GetCoverArt(string id)
        {
            var url = $"release-group/{id}";

            var result = await client.GetAsync(url);
            if (result.StatusCode != HttpStatusCode.OK)
                return null;

            return await result.Content.ReadAsAsync<CoverArt>();
        }
    }
}
