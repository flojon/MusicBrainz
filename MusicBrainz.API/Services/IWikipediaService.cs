using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public interface IWikipediaService
    {
        Task<QueryResult> GetExtract(string titles);
    }
}
