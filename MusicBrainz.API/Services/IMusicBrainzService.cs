using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public interface IMusicBrainzService
    {
        Task<Artist> GetArtist(string mbid);
    }
}