using System.Threading.Tasks;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public interface IWikiDataService
    {
        Task<WikiData> GetData(string id);
    }
}
