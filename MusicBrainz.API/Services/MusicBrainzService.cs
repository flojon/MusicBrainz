using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MusicBrainz.API.Services.Entities;

namespace MusicBrainz.API.Services
{
    public class MusicBrainzService: IMusicBrainzService
    {
        private readonly HttpClient client;

        public MusicBrainzService(HttpClient client)
        {
            this.client = client;
        }


        public async Task<Artist> GetArtist(string mbid)
        {
            var url =  $"artist/{mbid}?inc=url-rels+release-groups";

            var result = await client.GetAsync(url);
            if (result.StatusCode != HttpStatusCode.OK)
                return null;

            return await result.Content.ReadAsAsync<Artist>();
        }
    }
}
