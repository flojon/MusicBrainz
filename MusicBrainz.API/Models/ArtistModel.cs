using System.Collections.Generic;

namespace MusicBrainz.API.Models
{
    public class ArtistModel
    {
        public string Mbid { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public string StartedDate { get; set; }
        public string EndedDate { get; set; }
        public bool Ended { get; set; }
        public IEnumerable<AlbumModel> Albums { get; set; }
    }
}
