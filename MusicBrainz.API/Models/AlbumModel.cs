using System;

namespace MusicBrainz.API.Models
{
    public class AlbumModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string ReleaseDate { get; set; }
        public Uri Image { get; set; }
    }
}
