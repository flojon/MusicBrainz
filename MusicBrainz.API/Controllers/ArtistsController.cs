using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MusicBrainz.API.Models;
using MusicBrainz.API.Services;
using MusicBrainz.API.Services.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace MusicBrainz.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArtistsController: Controller
    {
        private readonly ICoverArtArchiveService coverArtArchiveService;
        private readonly IMemoryCache memoryCache;
        private readonly IMusicBrainzService musicBrainz;
        private readonly IWikiDataService wikidata;
        private readonly IWikipediaService wikipedia;

        public ArtistsController(
            IMusicBrainzService musicBrainz,
            IWikiDataService wikidata,
            IWikipediaService wikipedia,
            ICoverArtArchiveService coverArtArchiveService,
            IMemoryCache memoryCache
        )
        {
            this.musicBrainz = musicBrainz;
            this.wikidata = wikidata;
            this.wikipedia = wikipedia;
            this.coverArtArchiveService = coverArtArchiveService;
            this.memoryCache = memoryCache;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<ArtistModel>> Get(string id)
        {
            ArtistModel model;

            if (!memoryCache.TryGetValue(id, out model))
            {
                var artist = await musicBrainz.GetArtist(id);
                if (artist == null)
                    return NotFound("Not Found");

                model = new ArtistModel
                {
                    Mbid = artist.Id,
                    Name = artist.Name,
                    Country = artist.Country,
                    Description = await GetDescription(artist),
                    StartedDate = artist.LifeSpan?.Begin,
                    EndedDate = artist.LifeSpan?.End,
                    Ended = artist.LifeSpan?.Ended ?? false,
                    Albums = await GetAlbumsWithCoverArt(artist),
                };

                memoryCache.Set(id, model);//, DateTime.UtcNow.AddDays(1));
            };

            return Ok(model);
        }

        private async Task<string> GetDescription(Artist artist)
        {
            var id = artist.Relations.SingleOrDefault(r => r.Type == "wikidata").Url.Resource.Split('/').Last();

            var data = await wikidata.GetData(id);
            var title = data.SiteLinks["enwiki"].Title;

            var result = await wikipedia.GetExtract(title);
            if (result == null)
                return null;

            var page = result.Query?.Pages?.First();

            return page?.Value?.Extract;
        }

        private async Task<IEnumerable<AlbumModel>> GetAlbumsWithCoverArt(Artist artist)
        {
            var coverArt = artist.ReleaseGroups.Select(rg => new
            {
                Id = rg.Id,
                Title = rg.Title,
                ReleaseDate = rg.FirstReleaseDate,
                ImageRequest = coverArtArchiveService.GetCoverArt(rg.Id),
            }).ToList(); // Force evaluation of Linq by using ToList

            await Task.WhenAll(coverArt.Select(x => x.ImageRequest));

            var albums = coverArt.Select(c => new AlbumModel
            {
                Id = c.Id,
                Title = c.Title,
                ReleaseDate = c.ReleaseDate,
                Image = c.ImageRequest.Result?.Images.FirstOrDefault(x => x.Front)?.ImageUrl,
            });

            return albums;
        }
    }
}
