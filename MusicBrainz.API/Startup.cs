﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MusicBrainz.API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MusicBrainz.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMemoryCache();

            services.AddHttpClient<IMusicBrainzService, MusicBrainzService>(client =>
            {
                client.BaseAddress = Configuration.GetValue<Uri>("MusicBrainz:Url");
                client.DefaultRequestHeaders.Add("User-Agent", Configuration.GetValue<string>("MusicBrainz:AgentString"));
                client.DefaultRequestHeaders.Add("Accept", "application/json");
            });

            services.AddHttpClient<IWikipediaService, WikipediaService>(client =>
            {
                client.BaseAddress = Configuration.GetValue<Uri>("Wikipedia:Url");
            });

            services.AddHttpClient<ICoverArtArchiveService, CoverArtArchiveService>(client =>
            {
                client.BaseAddress = Configuration.GetValue<Uri>("CoverArtArchive:Url");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
            });

            services.AddHttpClient<IWikiDataService, WikiDataService>(client => {
                client.BaseAddress = Configuration.GetValue<Uri>("WikiData:Url");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
