# MusicBrainz API
This is my implementation of an assignment to create an API on top of the
MusicBrainz API

## Installation
This project uses the currently latest Release Candidate of .Net 2.1
which can be downloaded from here:
https://www.microsoft.com/net/download/dotnet-core/sdk-2.1.300-rc1

To use it in Visual Studio you need version 15.7 which was recently released.

### Certificate
The project uses HTTPS for secure connections.

### Visual Studio
Visual Studio will automatically generate a dev-certificate and ask you to trust it if it's not already trusted.

### CLI
To create and install a dev-certificate from the dotnet CLI use the following commands (works on Windows and macOS)
```
dotnet dev-certs https
dotnet dev-certs https -t
```

### Postman
If using Postman the connection will not be allowed even if the certificate is trusted. You need to go to settings and disable *SSL certificate verification* in the *General* section.

## Running
### Visual Studio
From Visual Studio you can just run the project as usual.
You may get a question about SSL certificate.

### CLI


If you're using the dotnet CLI you can run the project by going to the folder ```MusicBrainz.API``` and use the following commands:
```
dotnet restore
dotnet run
```

### Usage
The API currently only has one endpoint
```/api/artists/<mbid>``` where ```<mbid>``` is the artists id from MusicBrainz

As an example try this URL:
https://localhost:5001/api/artists/c85cfd6b-b1e9-4a50-bd55-eb725f04f7d5
which gives you information about the artist Avicii.

## Test
The project also has a few tests these can be run in Visual Studio using the Test Explorer or from the CLI by going to the folder ```MusicBrainz.API.Test``` and use the command:
```
dotnet test
```

## Caching
The API caches each request for 24 hours to try to limit the number of requests that are made to the external APIs. This means the first request for an artist will take a few seconds but the following requests for the same artist will go much faster.